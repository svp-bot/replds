package replds

import (
	"context"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"net/http/httptest"
	"os"
	"path/filepath"
	"testing"
	"time"
)

func init() {
	// Set values appropriate for testing.
	maxResponseSize = 64 * 1024
	pollPeriod = 3 * time.Second
}

type testCluster struct {
	baseDir     string
	dirs        []string
	servers     []*Server
	httpServers []*breakableServer
}

type breakableServer struct {
	*httptest.Server
	active bool
}

func newBreakableServer(h http.Handler) *breakableServer {
	s := &breakableServer{
		active: true,
	}
	s.Server = httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if !s.active {
			// Use a permanent error.
			http.Error(w, "broken", http.StatusNotFound)
			return
		}
		h.ServeHTTP(w, r)
	}))
	return s
}

func newTestCluster(t testing.TB, n int, baseDir string) *testCluster {
	var c testCluster

	c.baseDir = baseDir
	if baseDir == "" {
		dir, err := ioutil.TempDir("", "")
		if err != nil {
			t.Fatal(err)
		}
		baseDir = dir
	}

	// Create root directories.
	for i := 0; i < n; i++ {
		dir := filepath.Join(baseDir, fmt.Sprintf("fs%d", i))
		os.Mkdir(dir, 0755) // nolint
		c.dirs = append(c.dirs, dir)
	}

	var httpServers []*HTTPServer
	for i := 0; i < n; i++ {
		h := &HTTPServer{}
		httpServers = append(httpServers, h)
		c.httpServers = append(c.httpServers, newBreakableServer(h.Handler()))
	}
	for i := 0; i < n; i++ {
		s, err := NewServer(c.peersFor(i), c.dirs[i], nil, false)
		if err != nil {
			t.Fatal(err)
		}
		s.logger = log.New(os.Stderr, fmt.Sprintf("%d: ", i), log.LstdFlags)
		c.servers = append(c.servers, s)
		httpServers[i].Server = s
	}

	return &c
}

func (c *testCluster) setStates(states ...bool) {
	for i, active := range states {
		c.httpServers[i].active = active
	}
}

func (c *testCluster) setState(i int, active bool) {
	c.httpServers[i].active = active
}

func (c *testCluster) peersFor(idx int) []string {
	var out []string
	for i := 0; i < len(c.httpServers); i++ {
		if i != idx {
			out = append(out, c.httpServers[i].URL)
		}
	}
	return out
}

func (c *testCluster) client(t testing.TB, i int) *client {
	client, err := newClient(peerConfig(c.httpServers[i].URL, nil))
	if err != nil {
		t.Fatalf("newClient: %v", err)
	}
	return client
}

func (c *testCluster) Close() {
	for i := 0; i < len(c.servers); i++ {
		c.httpServers[i].Close()
		c.servers[i].Close()
	}
	if c.baseDir != "" {
		os.RemoveAll(c.baseDir)
	}
}

func TestCluster_SetNodes(t *testing.T) {
	s := newTestCluster(t, 3, "")
	defer s.Close()

	c := s.client(t, 0)
	resp, err := c.SetNodes(context.Background(), &SetNodesRequest{
		Nodes: []*Node{
			&Node{
				Path:      "test1",
				Value:     []byte("test content"),
				Timestamp: time.Now(),
			},
		},
	})
	if err != nil {
		t.Fatalf("SetNodes(): %v", err)
	}

	if resp.HostsErr > 0 || resp.HostsOk != 3 {
		t.Fatalf("bad setnodes response: %+v", resp)
	}

	for i, dir := range s.dirs {
		if _, err := os.Stat(filepath.Join(dir, "test1")); err != nil {
			t.Errorf("file has not been replicated to all nodes (missing: %d)", i)
		}
	}
}

func TestCluster_SetNodes_1Broken(t *testing.T) {
	s := newTestCluster(t, 3, "")
	defer s.Close()

	s.setStates(true, true, false)

	c := s.client(t, 0)
	resp, err := c.SetNodes(context.Background(), &SetNodesRequest{
		Nodes: []*Node{
			&Node{
				Path:      "test1",
				Value:     []byte("test content"),
				Timestamp: time.Now(),
			},
		},
	})
	if err != nil {
		t.Fatalf("SetNodes(): %v", err)
	}

	if resp.HostsErr != 1 || resp.HostsOk != 2 {
		t.Fatalf("bad setnodes response: %+v", resp)
	}

	s.setState(2, true)

	var ok bool
	deadline := time.Now().Add(30 * time.Second)
	for time.Now().Before(deadline) {
		if _, err := os.Stat(filepath.Join(s.dirs[2], "test1")); err == nil {
			ok = true
			break
		}
		time.Sleep(1 * time.Second)
	}
	if !ok {
		t.Fatal("propagation did not happen within the timeout")
	}
}

func TestCluster_PartialSync(t *testing.T) {
	s := newTestCluster(t, 2, "")
	defer s.Close()

	s.setStates(true, false)

	c := s.client(t, 0)
	// Create 10 files of 16k each. That should be enough to take
	// us above the test maxResponseSize.
	count := 10
	testVal := make([]byte, 16384)
	for i := 0; i < len(testVal); i++ {
		testVal[i] = 42
	}
	var req SetNodesRequest
	for i := 0; i < count; i++ {
		req.Nodes = append(req.Nodes, &Node{
			Path:      fmt.Sprintf("test%02d", i),
			Value:     testVal,
			Timestamp: time.Now(),
		})
	}
	if _, err := c.SetNodes(context.Background(), &req); err != nil {
		t.Fatalf("SetNodes(): %v", err)
	}

	// Now pull up the other server and watch it replicate.
	s.setState(1, true)

	var numNodes int
	deadline := time.Now().Add(30 * time.Second)
	for numNodes < count && time.Now().Before(deadline) {
		numNodes = len(s.servers[1].nodes)
		time.Sleep(1 * time.Second)
	}
	if numNodes < count {
		t.Fatalf("nodes did not replicate fully within the deadline (%d/%d)", numNodes, count)
	}
}
