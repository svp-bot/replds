module git.autistici.org/ai3/tools/replds

go 1.14

require (
	git.autistici.org/ai3/go-common v0.0.0-20210118064555-73f00db54723
	github.com/coreos/go-systemd/v22 v22.3.2
	github.com/google/subcommands v1.2.0
	github.com/prometheus/client_golang v1.11.0
	gopkg.in/yaml.v2 v2.4.0
)
